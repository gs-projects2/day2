import java.util.*;
class Solution {
    public int ArrayPairSum(int[] nums) {
       
        Arrays.sort(nums);
        int max_sum = 0;
        for(int i=0; i < nums.length; i += 2) {
            max_sum += nums[i];
        }
        return max_sum;
    }
}
        
    