class Solution {
    public int[] ShuffleArray(int[] nums, int n) {
         int j=0,i=0;
        int[] newNums=new int[2*n];
        for(i=0;i<2*n;i++){           
            if(i==n) j=1;
            newNums[j]=nums[i];
            j=j+2;
        }
        return newNums;        
    }
}