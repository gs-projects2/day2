class Solution {
    public List<String> StringMatching(String[] words) {
         
         List<String> list = Arrays.asList(words);
         Collections.sort(list, (p, q) -> p.length() - q.length());
         List<String> res = new ArrayList<>();
         for(int i = 0; i < words.length; ++i) {
             
             String word = list.get(i);
             for (int j = words.length - 1; j > i; --j) {
                 
                 if (list.get(j).contains(word)) {
                     
                     res.add(word);
                     break;
                     
                }
            }
        }
        
        return res;
    }
        
    }
