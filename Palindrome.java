class Solution {
    public boolean isPalindrome(int x) {
     
		 // convert input into an array and rest is nothing but a simple two pointer solution
           char[] nums = String.valueOf(x).toCharArray();
           int start = 0;
           int end = nums.length-1;
           while(start < end) {
           if(nums[start] != nums[end]) return false;
           start++; end--;
           }
           return true;
           }